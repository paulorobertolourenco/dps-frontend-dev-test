/**
 * TeamMembers Class definition
 */

(function ($) {

	"use strict";

	var TeamMember = function () {
		this.init();
	};

	TeamMember.prototype = (function () {

		return {
			constructor: TeamMember,

			init: function () {
			},

			showTooltip: function () {

			},

			hideTooltip: function () {

			},

			updateTooltipPosition: function () {

			}
		};
	})();

	$.fn.teamMember = function () {
		return $(this).each(function () {
			return new TeamMember(this);
		});
	};
})(window.jQuery);
