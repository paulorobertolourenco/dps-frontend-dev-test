<?php
function frontendTestAssets() {
	wp_enqueue_style('frontend_test_main', get_template_directory_uri() . '/assets/css/main.min.css', false, filemtime(get_template_directory() . '/assets/css/main.min.css'));

	// jQuery is loaded using the same method from HTML5 Boilerplate:
	// Grab Google CDN's latest jQuery with a protocol relative URL; fallback to local if offline
	// It's kept in the header instead of footer to avoid conflicts with plugins.
	if (!is_admin()) {
		wp_deregister_script('jquery');
		wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js', array(), null, false);
		add_filter('script_loader_src', 'frontendTestJqueryLocalFallback', 10, 2);
	}

	if (is_single() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}

	wp_register_script('modernizr', get_template_directory_uri() . '/assets/js/vendor/modernizr-2.7.0.min.js', array(), null, false);
	wp_register_script('frontend_test_scripts', get_template_directory_uri() . '/assets/js/scripts.min.js', array(), filemtime(get_template_directory() . '/assets/js/scripts.min.js'), true);
	wp_enqueue_script('modernizr');
	wp_enqueue_script('jquery');
	wp_enqueue_script('frontend_test_scripts');
}
add_action('wp_enqueue_scripts', 'frontendTestAssets', 100);

// http://wordpress.stackexchange.com/a/12450
function frontendTestJqueryLocalFallback($src, $handle = null) {
	static $add_jquery_fallback = false;

	if ($add_jquery_fallback) {
		echo '<script>window.jQuery || document.write(\'<script src="' . get_template_directory_uri() . '/assets/js/vendor/jquery-1.11.0.min.js"><\/script>\')</script>' . "\n";
		$add_jquery_fallback = false;
	}

	if ($handle === 'jquery') {
		$add_jquery_fallback = true;
	}

	return $src;
}
add_action('wp_head', 'frontendTestJqueryLocalFallback');
