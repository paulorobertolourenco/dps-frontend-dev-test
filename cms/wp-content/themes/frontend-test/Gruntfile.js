module.exports = function(grunt) {

	'use strict';

	var jsFileList = [
		'assets/js/*.js',
		'!assets/js/scripts.min.js'
	];

	require('load-grunt-tasks')(grunt);
	require('time-grunt')(grunt);

	grunt.initConfig({
		less: {
			dist: {
				files: {
					'assets/css/main.min.css': ['assets/less/main.less']
				},
				options: {
					compress: true,
					sourceMap: true,
					sourceMapFilename: 'assets/css/main.css.map',
					sourceMapRootpath: '/'
				}
			}
		},
		autoprefixer: {
			options: {
				browsers: ['last 2 versions', 'ie 8', 'ie 9', 'android 2.3', 'android 4', 'opera 12']
			},
			dist: {
				options: {
					map: {
						prev: 'assets/css/'
					}
				},
				src: 'assets/css/main.min.css'
			}
		},
		modernizr: {
			dist: {
				devFile: 'assets/vendor/modernizr/modernizr.js',
				outputFile: 'assets/js/vendor/modernizr.min.js',
				files: {
					'src': [
						['assets/js/scripts.min.js'],
						['assets/css/main.min.css']
					]
				},
				extra: {
					shiv: true
				},
				uglify: true,
				parseFiles: true
			}
		},
		copy: {
			fontawesome: {
				files: [
					{
						expand: true,
						cwd: 'assets/vendor/fontawesome/fonts/',
						src: ['**'],
						dest: 'assets/fonts/fontawesome'
					}
				]
			},
			jquery: {
				files: [
					{
						expand: true,
						cwd: 'assets/vendor/jquery/dist/',
						src: ['jquery.min.js'],
						dest: 'assets/js/vendor'
					}
				]
			}
		},
		uglify: {
			dist: {
				files: {
					'assets/js/scripts.min.js': [jsFileList]
				},
				options: {
					sourceMap: 'assets/js/scripts.min.js.map',
					sourceMappingURL: '/assets/js/scripts.min.js.map'
				}
			}
		},
		watch: {
			less: {
				files: [
					'assets/less/*.less',
					'assets/less/**/*.less'
				],
				tasks: ['less', 'autoprefixer']
			},
			js: {
				files: [jsFileList],
				tasks: ['uglify']
			},
			gruntfile: {
				files: [
					'Gruntfile.js'
				],
				tasks: ['default']
			},
			livereload: {
				options: {
					livereload: true
				},
				files: [
					'assets/css/main.min.css',
					'assets/js/scripts.min.js',
					'*.html'
				]
			}
		}
	});

	grunt.registerTask('default', [
		'less',
		'autoprefixer',
		'uglify'
	]);

	// Run this after installing or updating modernizr, fontawesome, jquery, etc.
	grunt.registerTask('build_base_assets', [
		'modernizr',
		'copy'
	]);
};
